# Requires Python 3.6+
from csv import DictReader
from sys import argv, exit
from datetime import datetime, timedelta
import json

satellites = {}
alerts = []


def maybe_issue_alert(red_readings, id, component):
    severity = "RED HIGH" if component == "TSTAT" else "RED LOW"
    if red_readings["third"] - red_readings["first"] < timedelta(minutes=5):
        alert = {
            "satelliteId": id,
            "severity": severity,
            "component": component,
            "timestamp": red_readings["first"].isoformat()[:-3]+"Z"
        }
        alerts.append(alert)


def add_red_reading(id, timestamp, component):
    dt = datetime.strptime(timestamp, "%Y%m%d %H:%M:%S.%f")
    if id not in satellites:
        # No records for this satellite
        satellites[id] = {component: {"first": dt}}
    elif component not in satellites[id]:
        # No records for this satellite component, add the first
        satellites[id][component] = {"first": dt}
    elif "second" not in satellites[id][component]:
        # Only one 'red' reading for this satellite component, add a second
        satellites[id][component]["second"] = dt
    else:
        # Two or more records for this satellite component
        if "third" in satellites[id][component]:
            # Three records already exist - advance the window
            satellites[id][component]["first"] = satellites[id][component]["second"]
            satellites[id][component]["second"] = satellites[id][component]["third"]
        # Store the third record
        satellites[id][component]["third"] = dt
        maybe_issue_alert(satellites[id][component], id, component)


def process_tstat_record(record):    
    if float(record["raw-value"]) > float(record["red-high-limit"]):
        # High TSTAT reading
        add_red_reading(record["satellite-id"], record["timestamp"], "TSTAT")


def process_batt_record(record):
    if float(record["raw-value"]) < float(record["red-low-limit"]):
        # Low BATT reading
        add_red_reading(record["satellite-id"], record["timestamp"], "BATT")


def main():
    if len(argv) < 2:
        exit("No input file specified")
    else:
        input_csv = argv[1]
        fields = ["timestamp", "satellite-id", "red-high-limit", "yellow-high-limit",
                  "yellow-low-limit", "red-low-limit", "raw-value", "component"]
        with open(input_csv, newline="") as csvfile:
            reader = DictReader(csvfile, delimiter="|", fieldnames=fields)
            for row in reader:
                if row["component"] == "TSTAT":
                    process_tstat_record(row)
                elif row["component"] == "BATT":
                    process_batt_record(row)
        print(json.dumps(alerts, indent=4))


if __name__ == "__main__":
    main()